var min=0.2;
var max=0.6;
var dog_cl = new Phaser.Class({
  Extends: Phaser.Scene,

  initialize:
  function SceneMainMenu ()
         {
             Phaser.Scene.call(this, {key: 'dog_cl' });
         },
  preload(){
    this.load.spritesheet('dog', 'assets/khol.png', { frameWidth: 1258, frameHeight: 938});
    this.load.image('platform','assets/platform.png');
  },
  create(){

  this.grouPlatform = this.physics.add.group();
  this.ball=this.physics.add.sprite(0,-400,'dog').setCollideWorldBounds(false);
  var firstplatform = this.add.sprite(0, 0, 'platform');
  this.grouPlatform.add(firstplatform);
  firstplatform.body.setImmovable(true);
  // свойства пса
  this.ball.setScale(0.05,0.05);
  this.ball.setGravityY(350);
  this.ball.speedX=0;
  this.ball.CamX=0;
  this.ball.jump=true;
  this.ball.doublejump=false;
  this.platMaX=0;
    this.anims.create({
        key: 'left',
        frames: this.anims.generateFrameNumbers('dog', { start: 0, end: 3 }),
        frameRate: 10,
        repeat: -1
    });

    this.anims.create({
        key: 'turn',
        frames: [ { key: 'dog', frame: 1 } ],
        frameRate: 20
    });

    this.anims.create({
        key: 'right',
        frames: this.anims.generateFrameNumbers('dog', { start: 1, end: 6 }),
        frameRate: 10,
        repeat: -1
    });
   //  Input Events
  this.cursors = this.input.keyboard.createCursorKeys();
  this.physics.add.collider(this.ball,this.grouPlatform);
  // camera
  this.cameras.main.startFollow(this.ball,true,0.1,0.01);
  this.cameras.main.followOffset.set(-this.ball.x/2-60,this.ball.y+60);
  // this.timedEvent = this.time.addEvent({
  //            delay: 500000,
  //            callback: this.addplatform,
  //            callbackScope: this,
  //            loop: true
  //        })
    for (var i = 0; i < 2; i++) {
      var last=this.grouPlatform.getLast(true);
      // console.log(last.x);
      var newX=last.x+500+Phaser.Math.Between(50,100);
      var platform = this.add.sprite(newX,Phaser.Math.Between(-10,50), 'platform');
      this.grouPlatform.add(platform);
      platform.body.setImmovable(true);
    }
    this.input.keyboard.on('keydown_UP',this.checkDoubleJump, this);
},
checkDoubleJump(){
     if (this.ball.jump&&this.ball.body.touching.down) {
       let high=170;
        this.jump(high);
         console.log(this.ball.jump);
         this.ball.jump=false;
         this.ball.doublejump=true;
         // this.ball.anims.play('jump');
     }
     else {
       if (this.ball.doublejump&&!(this.ball.body.touching.down)) {
         let high=130;
         this.jump(high);
         this.ball.jump=false;
         this.ball.doublejump=false;
         // this.ball.anims.play('doublejump');
       }
     }
},
jump(high){
  this.ball.setVelocityY(-high);
},
placeplatform(platformk,num){
  if(num==0){
    var last_p=this.grouPlatform.getFirstNth(3,true);
  }
  else if(num ==1){
    var last_p=this.grouPlatform.getFirstNth(2,true);
  }
  else if (num == 2){
    var last_p=this.grouPlatform.getFirstNth(1,true);
  }
platformk.setX(last_p.x+500+Phaser.Math.Between(50,100));
platformk.setY(Phaser.Math.Between(-10,50));
platformk.setScale((Math.random() * (max - min)) + min,1);
},
update(time,dt){
if(this.ball.body.touching.down){
      this.ball.jump=true;
      this.ball.doublejump=true;
      this.ball.speedX-=4;
      this.ball.CamX-=0.01;
  switch (true) {
    case this.ball.speedX<=0:
      this.ball.speedX=0;
      this.ball.CamX=0;
      this.ball.anims.play('turn');
      this.cameras.main.followOffset.set(-this.ball.x/2-60,this.ball.y+60);
      break;
    case this.ball.speedX>=360:
      this.ball.speedX=360;
      this.ball.CamX=0.9;
      break;
  }
  if(this.cursors.right.isDown){
    this.ball.anims.play('right', true);
    this.ball.speedX+=8;
    this.ball.CamX+=0.02;
    }
}
for (var i = 0; i < 3; i++) {
var ls=this.grouPlatform.getLastNth(i,true);
  if(ls.x<-300) {
    this.placeplatform(ls,i);
  }
}
this.grouPlatform.setVelocityX(-this.ball.speedX);
this.cameras.main.setZoom(1+1*this.ball.CamX);
}
});
var config = {
    type: Phaser.WEBGL,
    width: 1024,
    height: 768,
    backgroundColor: '#2a0b4c',
    scene: [dog_cl],
    physics: {
      default: 'arcade',
      arcade: {
          gravity: { y: 0 },
          debug: false
      }
    }
};

var game = new Phaser.Game(config);
