package main

import (
	"fmt"
	"io/ioutil"

	"github.com/valyala/fasthttp"
)

func main() {

	fasthttp.ListenAndServe(":80", func(ctx *fasthttp.RequestCtx) {

		filename := string(ctx.Request.URI().LastPathSegment())

		if filename == "" {
			filename = "index.html"
			ctx.Response.Header.SetContentType("text/html")
		} else {
			ctx.Response.Header.SetContentType("text/javascript")
		}

		h, _ := ioutil.ReadFile(filename)

		_, err := fmt.Fprintf(ctx, string(h))
		if err != nil {
			fmt.Println(err.Error())
		}
	})
}
